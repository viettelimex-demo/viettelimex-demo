package com.viettel.digital.claims.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PMKDemoController {

    @GetMapping("PMK-demo")
    public ResponseEntity<String> getLuckySpin(@RequestParam String username) {
        return new ResponseEntity<>(username + " Success", HttpStatus.OK);
    }

}
