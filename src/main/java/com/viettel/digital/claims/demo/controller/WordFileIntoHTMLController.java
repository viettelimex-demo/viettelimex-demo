package com.viettel.digital.claims.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class WordFileIntoHTMLController {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/word-to-html")
    public ResponseEntity<String> getLuckySpin(MultipartFile wordDocument) {

    	logger.info("Converting word document into HTML document");

//        ConvertedDocumentDTO htmlDocument = converter.convertWordDocumentIntoHtml(wordDocument);

        logger.info("Converted word document into HTML document.");
//        logger.trace("The created HTML markup looks as follows: {}", htmlDocument);
        return new ResponseEntity<>(" Success", HttpStatus.OK);
    }
}
